import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.util.Scanner
import kotlin.random.Random
import kotlin.io.path.*
import kotlin.system.exitProcess
import java.util.InputMismatchException

val scanner = Scanner(System.`in`)


var words = mutableListOf<String>()
var points = 0
var lives = 6
var hiddenWord = "diego"
val abc = "qwertyuiopasdfghjklzxcvbnm"
var wordList: MutableList<String> = mutableListOf()


fun main() {
    var language = selectLanguage()
    var sesion = login(language)
    wordleTitle()
    menu(sesion)


}

fun selectLanguage(): String {
    var currentSelection = "english"
    do {
        println(
            "${Colors.BG_BLACK}Select laguage by number:${Colors.reset}\n" +
                    "${Colors.BG_BLACK} 2\uFE0F⃣  English${Colors.reset}\n" +
                    "${Colors.BG_BLACK} 3\uFE0F⃣  Català${Colors.reset}\n" +
                    "${Colors.BG_BLACK} 4\uFE0F⃣  Castellano${Colors.reset}"

        )


        val selection = scanner.next()
        currentSelection = selection.toString()



    } while (selection != "2" && selection != "3" && selection != "4")
    when (currentSelection) {
        "2" -> {
            currentSelection = "english"
            println("${Colors.BG_BLACK}${Colors.LIGHT_WHITE}$currentSelection \uD83D\uDFE2${Colors.reset}\n" +
                    "${Colors.BG_BLACK}${Colors.LIGHT_WHITE}Insert your name:${Colors.reset}")
        }

        "3" -> {
            currentSelection = "catala"
            println("${Colors.BG_BLACK}${Colors.LIGHT_WHITE}$currentSelection \uD83D\uDFE2${Colors.reset}\n" +
                    "${Colors.BG_BLACK}${Colors.LIGHT_WHITE}Introdueix el teu nom:${Colors.reset}")
        }

        "4" -> {
            currentSelection = "castellano"
            println("${Colors.BG_BLACK}${Colors.LIGHT_WHITE}$currentSelection \uD83D\uDFE2${Colors.reset}\n" +
                    "${Colors.BG_BLACK}${Colors.LIGHT_WHITE}Introduce tu nombre:${Colors.reset}")
        }
    }
    loadWords(currentSelection)
    return currentSelection
}


fun login(language: String): Session {

    val name = scanner.next()

    val currentSession = Session(language, "$name")

    return currentSession
}

fun menu(currentSession: Session) {
    var menu: String? = null
    var option1: String? = null
    var option2: String? = null
    var option3: String? = null
    var option4: String? = null
    var option5: String? = null
    var invalidNumber: String? = null


    when (currentSession.language) {
        "english" -> {
            menu = "Welcome to Wordle ${currentSession.userName} "
            option1 = "1 - Play"
            option2 = "2 - Game data"
            option3 = "3 - Select language"
            option4 = "4 - Show instruction"
            option5 = "5 - Exit"
            invalidNumber = "Invalid option. Please enter a valid number."
        }

        "catala" -> {
            menu = "Benvingut/da a Wordle ${currentSession.userName} "
            option1 = "1 - Jugar"
            option2 = "2 - Historic de partides"
            option3 = "3 - Selecciona l'idioma"
            option4 = "4 - Mostrar instruccions"
            option5 = "5  - Exit"
            invalidNumber = "Nombre invalid. Si us plau introdueix un nombre valid"
        }

        "castellano" -> {
            menu = "Bienvenido a Wordle ${currentSession.userName} "
            option1 = "1 - Jugar"
            option2 = "2 - Historico de partidas"
            option3 = "3 - Selecciona el idioma"
            option4 = "4 - Mostrar instrucciones"
            option5 = "5 - Salir"
            invalidNumber = "Numero invalido. Por favor introduce un numero valido"
        }
    }

    var choice: Int

    do {
        println(
            "${Colors.BG_LIGHT_GRAY}${Colors.LIGHT_WHITE}$menu\n${Colors.reset}" +
                    "${Colors.BG_BLACK}$option1\n" +
                    "$option2\n" +
                    "$option3\n" +
                    "$option4 \n" +
                    "$option5\n " +
                    "${Colors.reset}"
        )

        try {
            choice = scanner.nextInt()
        } catch (e: InputMismatchException) {
            choice = -1 // Valor inválido para indicar una opción inválida
            scanner.nextLine() // Limpiar el búfer de entrada
        }

        when (choice) {
            1 -> {
                playWordle(currentSession.language, currentSession)
            }

            2 -> {
                showHistoricData()
            }

            3 -> {
                var language = selectLanguage()
                currentSession.language = language
                menu(currentSession)
            }

            4 -> {
                showInstructions(currentSession.language)
            }

            5 -> {
                exitProcess(0)
            }

            else -> {
                println(invalidNumber)
            }
        }
        println()

    } while (choice != 5)
    scanner.close()
}

fun playWordle(laguage: String, currentSession: Session) {
    points = 0
    var inputListenerMessage: String? = null
    var invalidWord: String? = null
    var winMessage: String? = null
    var pointsInfo: String? = null
    var gameOverMessage: String? = null
    var liveMessage: String? = null
    when (laguage) {
        "english" -> {
            inputListenerMessage = "Introduce a word"
            invalidWord = "You introduced invalid word, try again"
            winMessage = "Graet! You win with "
            pointsInfo = "You have "
            gameOverMessage = "Oh! no. Game over"
            liveMessage = "Lifes "
        }

        "catala" -> {
            inputListenerMessage = "Introduiex una paraula"
            invalidWord = "Has introduit una paraula invalida, torn a provar"
            winMessage = "Molt be! Has guanyat amb  "
            pointsInfo = "Tens "
            gameOverMessage = "Oh no. Has perdut"
            liveMessage = "Vides "
        }

        "castellano" -> {
            inputListenerMessage = "Introduce una palabra"
            invalidWord = "Has introducido una palabra inválida, prueba de nuevo"
            winMessage = "Enhorabuena! Ganaste con "
            pointsInfo = "Tienes "
            gameOverMessage = "Oh no. Has perdido"
            liveMessage = "vidas "
        }
    }

    while (lives > 0) {
        println("${Colors.BG_BLACK}$inputListenerMessage${Colors.reset}")
        var userWord = getUserInput()

        if (userWord == null) {
            println("${Colors.BG_RED}${Colors.BLACK}$invalidWord${Colors.reset}")
            continue
        }

        val result = compareWords(hiddenWord, userWord, Colors.yellow, Colors.green, Colors.gray, Colors.reset)

        if (result.second) {
            points += lives * 113
            wordList.add(result.first)
            for (i in wordList.indices) {
                println(wordList[i])
            }
            println("${winMessage} ${points}")
            saveData(currentSession.userName, wordList, points)
            wordList.clear()
            break
        } else {
            wordList.add(result.first)
            for (i in wordList.indices) {
                println(wordList[i])
            }
            println(
                "${Colors.BG_BLACK}${Colors.LIGHT_WHITE}${pointsInfo} ${points} pts\n"
                        + "$lives $liveMessage${Colors.reset}"
            )
            lives--
        }
    }

    if (lives == 0) {
        println(gameOverMessage)
    }
}

/**
 * Esta funcion escoge entre una serie de palabras almacenadas en un array.
 * @author Diego Arteaga
 * @param words Es un array que almacena las palabras que el ususario tratara de adivinar.
 * @return Retorna la palabra escogida aleatoriamente con la funcion Random
 */
fun getRandomWord(words: Array<String>) = words[List(1) { Random.nextInt(0, words.size) }[0]]

/**
 * Esta funcion comprueba que la palabra del ususario contenga solo letras y sea de la misma medida que la palabra oculta. En este caso 5.
 * @author Diego Arteaga
 * @param scanner es la palabra que el usuario introduce por consola.\
 * @param abc es un string que contiene cada letra del abecedario para poder comparar con la palabra introducida por el usuario
 * @return Retorna un string o null dependiendo si la palabra es valida para el juego o no.
 */
fun getUserInput(): String? {
    val userInput = scanner.next().toLowerCase()
    var itsIn = 0
    for (i in userInput.indices) {
        if (abc.contains(userInput[i])) {
            itsIn++
        }
    }
    return if (userInput.length == 5 && itsIn == 5) userInput else null
}

/**
 * Esta funcion compara la palabra introducida por el usuario con la palabra oculta escogida por el programa. Luego la printa segun
 * si es correcta o no, pintando de diferentes colores las letras.
 * @author Diego Arteaga
 * @param hiddenWord Es la palabra escogida por el programa con la que se compara la palabra introducida por el usuario.
 * @param userWord Es la palabra introducida por el usuario que se compara con la palabra oculta escogida por el programa.
 * @param yellow Es el codigo para printar una letra de amrillo.
 * @param green Es el codigo para printar una letra de verde.
 * @param gray Es el codigo para printar una letra de gris.
 * @param reset Es el codigo para printar una letra sin color.
 * @return Retorna la palabra escogida aleatoriamente con la funcion Random
 */
fun compareWords(
    hiddenWord: String,
    userWord: String,
    yellow: String,
    green: String,
    gray: String,
    reset: String
): Pair<String, Boolean> {
    val result = StringBuilder()
    var win = false
    val compared = mutableListOf<Char>()
    var count = 0
    for (i in hiddenWord.indices) {

        if (userWord == hiddenWord) {
            result.append("$green${hiddenWord[i]}$reset")
            points += 463
            win = true

        } else if (hiddenWord.contains(userWord[i]) && hiddenWord[i] == userWord[i]) {
            result.append("$green${userWord[i]}$reset")


        } else if (hiddenWord.contains(userWord[i]) && hiddenWord[i] != userWord[i]) {

            if (repetitions(userWord, userWord[i]) <= repetitions(hiddenWord, userWord[i])) {
                result.append("$yellow${userWord[i]}$reset")
                points += 33
            } else if (count < repetitions(hiddenWord, userWord[i]) - timesGuessed(hiddenWord, userWord, userWord[i])) {
                result.append("$yellow${userWord[i]}$reset")
                count++
                points += 33
            } else result.append("$gray${userWord[i]}$reset")
            points -= 4

        } else {
            result.append("$gray${userWord[i]}$reset")
            points -= 4
        }
    }
    return Pair(result.toString(), win)
}

fun repetitions(word: String, character: Char): Int {
    var n = 0
    for (char in word.indices) {
        if (character == word[char]) n++
    }
    return n
}

fun timesGuessed(firstWord: String, secondWord: String, character: Char): Int {
    var n = 0
    for (i in firstWord.indices) {
        if (firstWord[i] == secondWord[i] && firstWord[i] == character && secondWord[i] == character) {
            n++
        }
    }
    return (n)
}


fun saveData(userName: String, words: MutableList<String>, points: Int) {

    val file = File("./src/data/gameHistoric.txt")
    file.appendText("Player: $userName - $points pts. Words:")
    for (i in words.indices) {
        file.appendText("- ${words[i]} ")
    }
    file.appendText("\n")

}

fun showHistoricData() {
    val file = File("./src/data/gameHistoric.txt")
    BufferedReader(FileReader(file)).use {
        it.lines().forEach { println(it) }
    }
}

fun loadWords(laguage: String) {
    words.clear()
    val file = File("./src/data/${laguage}words.txt")
    BufferedReader(FileReader(file)).use {
        it.lines().forEach { words.add(it) }
    }
    hiddenWord = words.random()
}


fun showInstructions(laguage: String) {
    var instructions: String? = null

    when (laguage) {
        "english" -> {
            instructions =
                "${Colors.BG_LIGHT_GRAY}${Colors.BLACK} Wordle is a word-guessing game where the objective is to guess a hidden word \n" +
                        "  within a limited number of attempts. Here are the instructions to play Wordle:\n\n" +
                        "${Colors.BG_BLACK}${Colors.LIGHT_WHITE}  1: The game of Wordle consists of a hidden 5-letter word. Your goal is to guess \n" +
                        "  the correct word in as few attempts as possible.\n\n" +
                        "  2: On each attempt, you will provide a 5-letter word as a guess. You can use any \n" +
                        "  letter from the alphabet.\n\n" +
                        "  3: After each attempt, you will receive clues about your guess in the form of \"correct letters\" \n" +
                        "  and \"incorrect letters\". The correct letters are those that are in the correct position \n" +
                        "  in the hidden word. The incorrect letters are those that are in the hidden word but \n" +
                        "  in the wrong position.\n\n" +
                        "  4: Use the clues you receive to adjust your guesses. For example, if you receive the clue \n" +
                        "  that you have a correct letter but in the wrong position, you can try different \n" +
                        "  combinations of letters in that position.\n\n" +
                        "  5: You have a limited number of attempts to guess the correct word. The number of attempts: 6.\n\n" +
                        "  6: If you guess the correct word within the allowed number of attempts, you win the game. \n" +
                        "  If you fail to guess the correct word within the allotted attempts, you lose, and the hidden \n" +
                        "  word is revealed.\n\n" +
                        "  Remember that Wordle is a game of logic and deduction, so pay attention to the clues you receive \n" +
                        "  and use strategies to narrow down the possibilities and find the correct word. Enjoy playing Wordle!${Colors.reset}"
                  }

        "catala" -> {
            instructions =
                "${Colors.BG_LIGHT_GRAY}${Colors.BLACK} Wordle és un joc d'endevinar paraules on l'objectiu és endevinar una paraula oculta \n" +
                        "  en un nombre limitat d'intents. Aquí tens les instruccions per jugar a Wordle:\n\n" +
                        "${Colors.BG_BLACK}${Colors.LIGHT_WHITE}  1: El joc de Wordle consisteix en una paraula oculta de 5 lletres. El teu objectiu és endevinar \n" +
                        "  la paraula correcta amb el menor nombre d'intents possible.\n\n" +
                        "  2: En cada intent, hauràs de proporcionar una paraula de 5 lletres com a suposició. Pots \n" +
                        "  utilitzar qualsevol lletra de l'alfabet.\n\n" +
                        "  3: Després de cada intent, rebràs pistes sobre la teva suposició en forma de \"lletres correctes\" \n" +
                        "  i \"lletres incorrectes\". Les lletres correctes són aquelles que estan a la posició correcta \n" +
                        "  a la paraula oculta. Les lletres incorrectes són aquelles que estan a la paraula oculta però \n" +
                        "  a la posició equivocada.\n\n" +
                        "  4: Utilitza les pistes que rebeu per ajustar les teves suposicions. Per exemple, si reps la pista \n" +
                        "  que tens una lletra correcta però a la posició equivocada, pots provar diferents \n" +
                        "  combinacions de lletres en aquella posició.\n\n" +
                        "  5: Tens un nombre limitat d'intents per endevinar la paraula correcta. El nombre d'intents : 6.\n\n" +
                        "  6: Si endevines la paraula correcta dins el nombre d'intents permès, guanyes el joc. \n" +
                        "  Si no aconsegueixes endevinar la paraula correcta dins els intents assignats, perds i es revela \n" +
                        "  la paraula oculta.\n\n" +
                        "  Recorda que Wordle és un joc de lògica i deducció, així que presta atenció a les pistes que \n" +
                        "  rebes i utilitza estratègies per reduir les possibilitats i trobar la paraula correcta. Gaudeix jugant a Wordle!${Colors.reset}"
        }

        "castellano" -> {
            instructions =
                "${Colors.BG_LIGHT_GRAY}${Colors.BLACK} Wordle es un juego de adivinanzas de palabras donde el objetivo es adivinar una palabra oculta \n" +
                        "  dentro de un número limitado de intentos. Aquí están las instrucciones para jugar a Wordle:\n\n" +
                        "${Colors.BG_BLACK}${Colors.LIGHT_WHITE}  1: El juego de Wordle consiste en una palabra oculta de 5 letras. Tu objetivo es adivinar \n" +
                        "  la palabra correcta en el menor número de intentos posible.\n\n" +
                        "  2: En cada intento, deberás proporcionar una palabra de 5 letras como suposición. Puedes \n" +
                        "  utilizar cualquier letra del alfabeto.\n\n" +
                        "  3: Después de cada intento, recibirás pistas sobre tu suposición en forma de \"letras correctas\" \n" +
                        "  y \"letras incorrectas\". Las letras correctas son aquellas que están en la posición correcta \n" +
                        "  en la palabra oculta. Las letras incorrectas son aquellas que están en la palabra oculta pero \n" +
                        "  en la posición equivocada.\n\n" +
                        "  4: Utiliza las pistas que recibas para ajustar tus suposiciones. Por ejemplo, si recibes la pista \n" +
                        "  de que tienes una letra correcta pero en la posición equivocada, puedes probar diferentes \n" +
                        "  combinaciones de letras en esa posición.\n\n" +
                        "  5: Tienes un número limitado de intentos para adivinar la palabra correcta. El número de intentos: 6\n\n" +
                        "  6: Si adivinas la palabra correcta dentro del número de intentos permitidos, ganas el juego. \n" +
                        "  Si no logras adivinar la palabra correcta dentro de los intentos asignados, pierdes y se revela \n" +
                        "  la palabra oculta.\n\n" +
                        "  Recuerda que Wordle es un juego de lógica y deducción, así que presta atención a las pistas que \n" +
                        "  recibes y utiliza estrategias para reducir las posibilidades y encontrar la palabra correcta. ¡Disfruta jugando Wordle!${Colors.reset}"
        }
    }
    println(instructions)
}

fun wordleTitle() {
    println(
        "${Colors.BG_BLACK}${Colors.LIGHT_WHITE}" +
                " ▄█     █▄   ▄██████▄     ▄████████ ████████▄   ▄█          ▄████████ \n" +
                "███     ███ ███    ███   ███    ███ ███   ▀███ ███         ███    ███ \n" +
                "███     ███ ███    ███   ███    ███ ███    ███ ███         ███    █▀  \n" +
                "███     ███ ███    ███  ▄███▄▄▄▄██▀ ███    ███ ███        ▄███▄▄▄     \n" +
                "███     ███ ███    ███ ▀▀███▀▀▀▀▀   ███    ███ ███       ▀▀███▀▀▀     \n" +
                "███     ███ ███    ███ ▀███████████ ███    ███ ███         ███    █▄  \n" +
                "███ ▄█▄ ███ ███    ███   ███    ███ ███   ▄███ ███▌    ▄   ███    ███ \n" +
                " ▀███▀███▀   ▀██████▀    ███    ███ ████████▀  █████▄▄██   ██████████ \n" +
                "                         ███    ███            ▀                      \n" +
                "${Colors.reset}                                                                      "
    )
}