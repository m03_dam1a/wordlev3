
object Others {
    const val ABC: String = "qwertyuiopasdfghjklzxcvbnm"
    const val BLACK: String = "\u001b[30m"
    const val RED: String = "\u001b[31m"
    const val GREEN: String = "\u001b[32m"
    const val YELLOW: String = "\u001b[33m"
    const val BLUE: String = "\u001b[34m"
    const val MAGENTA: String = "\u001b[35m"
    const val CYAN: String = "\u001b[36m"
    const val LIGHT_GRAY: String = "\u001b[37m"

}